# geradorname

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Info
O modelo do Frontend contruindo atraves do curso do [Rodrigo Branas](https://www.youtube.com/watch?v=zhbOh6zFCuc&list=PLQCmSnNFVYnTiC-pPY0SySbf-ZNGBwnaG&index=2).
Mas o uso de componente esta sendo utilizada de formas diferente.
No curso grande parte da contrução esta no arquivo app, ja eu dividir em componentes para me aprofundar no framework

### Production
Projeto com a versão de deploy disponivel no netlifly no [gerador de nomes](https://gerador-nomes.netlify.com/).

### API
Laravel 
{
    - Criado backend em php com laravel para aplicação frontend consumir os dados.
    - Banco de dados Sqlite.
    - url do repositorio pode ser encontrado [aqui](https://bitbucket.org/GabrielHs/api-nomes/src/master/ ). 
} 

Web api C# 
{
    - Criado backend em c# com webapi em .net core para aplicação frontend consumir os dados.
    - Banco de dados Sqlite.
    - Padrao repository.
    - url do repositorio pode ser encontrado [aqui](https://bitbucket.org/GabrielHs/api-nomes-c/src/master/ ). 
} 

# Important
Consulta de dominios é realizada via web scrapping no site do whois, caso seja feito atualizações do site, será necessario atualizar o projeto 