import Vue from 'vue'
import Router from 'vue-router'
import FixoDetalhe from '@/components/FixoDetalhe'
import NotFound from '@/components/NotFound'
import Fixo from "@/components/Fixo";

Vue.use(Router);

export default new Router({
    // mode: 'history',
    routes: [
        {
            path:'/',
            redirect:'/dominios'
        },
        {
            path:'/dominios',
            nome:'/dominios',
            component:Fixo
        },
        {
            path:'*',
            nome:'*',
            component:NotFound
        },
        {
            path:'/detalhe/:domain',
            nome:'detalhe',
            component:FixoDetalhe,
            props: true
        },

    ]
})